#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /release
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["myWebApp.csproj", "."]
RUN dotnet restore "./myWebApp.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "myWebApp.csproj" -c Release -o /release/build

FROM build AS publish
RUN dotnet publish "myWebApp.csproj" -c Release -o /release/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /release/publish .
ENTRYPOINT ["dotnet", "myWebApp.dll"]