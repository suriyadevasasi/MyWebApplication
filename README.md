Steps to set up the project and run it in a docker container (Windows Only).

1. Please clone the project.
2. Please open the .sln file with the visual studio version that you have installed in your local machine.
3. Please change the docker container to be Linux container and make sure you have restarted yor docker to avoid issues.
4. After the project is loaded in the visual studio, you can see the Docker button wil be visible on the toolbar.
6. Please click on the docker button for Visual Studio to  build and run your container image, and launch a web browser showing your web app running inside the newly created container.

Steps to set up the project and run it in a docker container (Linux Only).

1. Please download the project using this link 'https://gitlab.com/suriyadevasasi/MyWebApplication/-/archive/master/MyWebApplication-master.zip'.
2. Please install unzip with the command 'apt-get install unzip'.
3. unzip the project to your preferred directory.
4. navigate to the project folder and run the 'automatebuild.sh' file.
