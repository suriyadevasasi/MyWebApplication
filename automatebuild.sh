#!/bin/bash

IMGNAME=suriya/webapp

# build docker image
sudo docker build -t $IMGNAME .

# Deploy to Docker Container
sudo docker run -it -p 80:80 $IMGNAME web